#!/usr/bin/python

# pre-commit
# Given the README_prefix, README_postfix, and packaged_apps.json files, automatically build and create the README.md for this repo

# In order to use properly, copy this file to your .git/hooks directory as `pre-commit`, no extension

import json, subprocess

readme = ""

with open('packaged_apps.json', 'r') as apps_file, open('README_prefix.md', 'r') as rm_prefix_file, open('README_postfix.md', 'r') as rm_postfix_file, open('README.md', 'w') as readme_file:
  readme_prefix = rm_prefix_file.read()
  readme_postfix = rm_postfix_file.read()

  readme += readme_prefix

  data = apps_file.read()
  json_data = json.loads(data)
  apps = json_data['apps']

  for app in apps:
    more_links = ""
    if 'more_links' in app and len(app['more_links']) > 0:
      more_links += "("
      for idx, link in enumerate(app['more_links']):
        more_links += "[{}]({})".format(link['name'], link['url'])
        if idx < len(app['more_links']) - 1:
          more_links += ", "
      more_links += ")"

    package_features = ""
    if 'package_features' in app and len(app['package_features']) > 0:
      for idx, feature in enumerate(app['package_features']):
        package_features += "  - [{}] {}".format('x' if feature['value'] == True else ' ', feature['key'].capitalize())
        if idx < len(app['package_features']) - 1:
          package_features += "\n"

    notes = ""
    if 'notes' in app:
      notes = "  - NOTES: {}\n".format(app['notes'].encode('utf-8'))

    app_md = '''
- [{name}]({repo_url}) - {description} {more_links}
{package_features}
{notes}'''.format(name=app['name'], repo_url=app['repo_url'], description=app['description'], more_links=more_links, package_features=package_features, notes=notes)

    readme += app_md

  readme +="\n\n{}".format(readme_postfix)

  readme_file.write(readme)

subprocess.check_output(["git", "add", "README.md" ])
print("Updated README file")