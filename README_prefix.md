# Cloudron Packaged Apps

The [Cloudron App Store](https://www.cloudron.io/store/index.html) is the go-to place for Apps professionally packaged for Cloudron. However, sometimes there are Apps that don't really belong in the official store. For those, there is this repo! 
