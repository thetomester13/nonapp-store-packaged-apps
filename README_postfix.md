## Contributing

Just as the packages listed here consist of developers working hard, this list too is reliant on contributors to add any packages in the wild they're aware of and belong here. 

### Step to Contribute
1. Fork repo and clone. 
2. Be sure you have the pre-commit hook installed properly on your machine. This is what builds out the final README.md file before it gets pushed up. 
    - Copy the `pre-commit.py` file to `.git/hooks/pre-commit`, with no extension. 
    - Mark it executable, `chmod +x .git/hooks/pre-commit`
3. Update the `packaged_apps.json` file with the appropriate app data. View previously listed apps for formatting. 
4. Push up to your fork. 
5. Create a Merge Request into `master`! 
6. Feel free to ping [me](https://forum.cloudron.io/user/thetomester13) on the [Cloudron forum](https://forum.cloudron.io/) for merging or any other questions! 
