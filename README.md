# Cloudron Packaged Apps

The [Cloudron App Store](https://www.cloudron.io/store/index.html) is the go-to place for Apps professionally packaged for Cloudron. However, sometimes there are Apps that don't really belong in the official store. For those, there is this repo! 

- [Homepage](https://git.cloudron.io/thetomester13/homepage-app) - A simple, standalone, self-hosted PHP page that is your window to your server and the web. ([Source Code](https://github.com/tomershvueli/homepage-cloudron))
  - [x] Tests
  - [x] License

- [Standard Notes (Sync Server)](https://git.cloudron.io/thetomester13/standardnotes-app) - The Standard Notes syncing server ([Source Code](https://github.com/standardnotes/syncing-server))
  - [ ] Tests
  - [ ] License

- [Standard Notes Web](https://git.cloudron.io/thetomester13/standardnotes-web-app) - A free, open-source, and completely encrypted notes app. ([Source Code](https://github.com/standardnotes/web))
  - [ ] Tests
  - [ ] License
  - NOTES: For some reason, it seems that this app doesn't backup well and often errors out in the process with a MySQL error ¯\_(ツ)_/¯ I just disabled the backing up of this app since the notes themselves are backed up in the Sync Server app. 

- [webhooks](https://git.cloudron.io/fbartels/webhook-app/) - webhook is a lightweight incoming webhook server to run shell commands ([Source Code](https://github.com/adnanh/webhook))
  - [ ] Tests
  - [ ] License


## Contributing

Just as the packages listed here consist of developers working hard, this list too is reliant on contributors to add any packages in the wild they're aware of and belong here. 

### Step to Contribute
1. Fork repo and clone. 
2. Be sure you have the pre-commit hook installed properly on your machine. This is what builds out the final README.md file before it gets pushed up. 
    - Copy the `pre-commit.py` file to `.git/hooks/pre-commit`, with no extension. 
    - Mark it executable, `chmod +x .git/hooks/pre-commit`
3. Update the `packaged_apps.json` file with the appropriate app data. View previously listed apps for formatting. 
4. Push up to your fork. 
5. Create a Merge Request into `master`! 
6. Feel free to ping [me](https://forum.cloudron.io/user/thetomester13) on the [Cloudron forum](https://forum.cloudron.io/) for merging or any other questions! 
